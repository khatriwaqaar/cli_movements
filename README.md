### Task Description

This is a basic command-line interface (CLI) program that moves a robot from its initial location according to a set of instructions. Once the robot completes its movement, the program displays the distance between the robot's final position and its starting point. The distance represents the shortest distance the robot needs to travel to return to its original position. The robot can only make 90-degree turns and can move in four cardinal directions: north, south, east, and west.

We have a robot that can receive commands in order to move it.  These commands will tell the robot to go forwards or backwards, and turn left or right.  These commands will be in the format '\<command>\<number>'. For example 'L1' means 'turn left by 90 degrees once'.  'B2' would mean go backwards 2 units.


### Available commands:
* `F` - move forward 1 unit
* `B` - move backward 1 unit
* `R` - turn right 90 degrees
* `L` - turn left 90 degrees

#### CLI

```bash
# CLI command
python robot.py -i F1,R1,B2,L1,B3

# Output would be
4 steps
```

### Running on local system:
###### Prerequisites
- Make sure Python is installed.
- Make sure that pytest is installed.
    ```bash
    pip install pytest
    ```
- Clone repo in on the localsystem
    ```bash
    cd Desktop/
    ```
    ```bash
    git clone https://gitlab.com/khatriwaqaar/cli_movements.git
    ```
- cd into the repo on desktop
    ```bash
    cd cli_movements
    ```
- Run the following command to run it the script
    ```bash
    python moves.py -i F1,R1,B2,L1,B3
    ```

## Design Decisions
This task was executed keeping simplicity and extensibility in mind. A robot class has been created to cover all the robot related tasks such as movment, rotation, distance calculation of the robot. Even though this is simple application, adding functionality is very simple in this application. 

For specific function description and logic please refer to [moves.py](https://gitlab.com/khatriwaqaar/cli_movements/-/blob/main/moves.py "moves.py").

The testing branch played a crucial role in the development of this project. The logic was implemented and refined on this branch, and local tests were conducted to ensure that the code was functioning as intended. To indicate the tests' completion on a specific commit, a trailing tag was added to the commit message, such as "Tested OK" or "Feaeture Merge" was used to indicate completion of a feature while merging to the main branch. Pytest was used for unit testing, and a range of dummy inputs were included to facilitate comprehensive testing, including erroneous inputs.

Once the logic was deemed to be fully functional, the code was restructured and further tests were added. The testing branch was then merged with the main branch, with the "Deployment Ready Merge" tag to signify that the code was ready for deployment.

The deployment branch was then created from the main branch, once the "Deployment Ready Merge" tag was visible in the commit history. After this, the Dockerfile and requirement.txt files were added. An image was built on a local system to test that it was suitable for use in CI/CD stages.
- #### Assumptions
There are some assumptions in building the project:

 1. The robot can only move in four directions (North, East, South, West).
 2. The robot can only move 1-9 units in a given command. 
 3. Robot's direction is not considered during calculation of the robot's distance from the starting point. For example if the robot is given a command "F3,R1,F2", the robot will be facing East after the robot has traversed. However, to go back to the start location, the robot has to turn to south direction to go back to the start location. This phenomenon is not incorporated in the distance calulation.
    
- #### Testing image build and script execution using Docker on Local system
    - Make sure Docker Deamon is running.
    - Use [Dockerfile](https://gitlab.com/khatriwaqaar/cli_movements/-/blob/main/Dockerfile "Dockerfile") to build on localsystem if required. 
        execute these command to test if the app runs on a container.
        ```bash
        docker build -t cli_movement . 
        docker run -it cli_movement:latest /bin/bash
        pytest _test.py -s 
        # This makes sure that pytest present is 'requirements.txt' was installed while building the image.
        # once the test finishes with PASS status we has established that test works.
        # Now to run application using command from cli
        python moves.py -i F1,R1,B2,L1,B3 # Output should be '4 steps'
        ```
- #### Pulling image from DockerHub:
    - Make sure Docker Deamon is running.
    - To pull image -- I have used Docker Hub for image repository. the image can be found at ``` https://hub.docker.com/r/waqaarkhatri/cli_movement ```
    - ```docker pull waqaarkhatri/cli_movement:latest```

### Repository Organization, Containerization and CI/CD Explaination:
- From the interview, I picked up that at Pronto, we use GitLab for code management and GitLab CI/CD
- I have used GitLab for as code repository and GitLab's CI/CD tool.
- I have maintained 3 branches on the repository. This in an ideal scenario would be multiple teams working together on different stages of the application. You can explore the Git Commit History of the   
##### Repo:
1.) ```testing``` branch:
- Logic was built on this branch. Once local tests were completed, commits were tagged with specific traling tags in commit message. Eg: - Tested OK etc.
- Used pytest for unit test. Also added some dummy inputs so that multiple inputs including wrong inputs can be tested OK.
- Once the logic was ready - working fine, code was restructured and tests were added.
- Testing branch was merged with main branch with 'Deployment Ready Merge - ' tag.

2.) ```deployment``` branch:
- Once we see 'Deployment Ready Merge' in the commit history. Deployment brach was created from main branch.
- After that Dockerfile, requirement.txt files were added.
- On testing image build on local system. It was good to go for CI/CD stages.
##### CI CD:
- .gitlab-ci.yml was created with test and build stage.
- Indivudal stages are seperated out so that none of them start together.
From the screen shot I can be seen that image is pushed to the DockerHub Repository after build is succeeded
- I have used variables ```$DOCKER_USER and $DOCKER_PASS``` for login with username and password before pushing to DockerHub container repository.
- GitLab uses Server and Runners for each stage of the CICD application. By default it uses ruby, hence I have explicitly specified ```docker:20.10.16``` image in build stage. 
- Now, On GitLab, by default - Each stage is a docker container running a runner machine. So if we build a docker container inside a docker container we need to make sure that docker deamon is avaialble. This is refered to as "DockerOnDocker" in Docker and GitLab Documentations.
- Hence, we need services to start before actual docker build is kicked of. 
- ```docker:20.10.16-dind``` was used to make sure taht docker deamon is available. Please note: 'dind' in tag refers to 'docker-in-docker'

### Future Expandable Capabilty:
#### Infrasture wise: 
- This application can be attached to API Server using Django/Flask Framework.
- Can be hosted on Gunicorn server to keep it lightweighted.
#### Deployment wise:
- Deployment can be done on kubelets running gunicorn for serving requests from the web app.
- .gitconfig.yml file can be edited to add another ```deploy``` stage.
    

        


## File Structure
- [moves.py](https://gitlab.com/khatriwaqaar/cli_movements/-/blob/main/moves.py "moves.py") - This script is the primary implementation in Python for the command-line interface (CLI) application that allows a robot to traverse and calculates the distance covered.
- [_test.py](https://gitlab.com/khatriwaqaar/cli_movements/-/blob/main/_test.py "_test.py") - This Python script is meant for testing the CLI application by running test cases provided within [dummy_input.json](https://gitlab.com/khatriwaqaar/cli_movements/-/blob/main/dummy_input.json "dummy_input.json") -- Here are some samples that are used for testing in the Pipeline.

