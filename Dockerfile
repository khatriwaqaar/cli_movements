FROM python:3.9-slim-buster

RUN pip install --upgrade pip

RUN mkdir cli_movements
WORKDIR /cli_movements
COPY ./ .

RUN pip install -r requirements.txt