import argparse
import sys

#        F=N 
#  L=W <--|--> R=E
#        B=S

class cli:
    """
    This class contains all function that run through logic.
    """
    def __init__(self,input_list):
        """
        Basic Initialisations.
        ** ASSUMPTIONS **
         - We have assumed, starting position of the 'robot' to be 0,0 and initial direction of the robot to be NORTH direction.
        """
        self.direction = 'N'
        self.position_a = 0 
        self.position_b = 0 #(a,b) = (0,0)
        for each_move in input_list:
            if each_move[0] in ['F', 'B']:
                self.forward_backward(each_move)
            elif each_move[0] in ['L', 'R']:
                self.rotate(each_move)  
    def rotate(self,each_move):
        """
        Logic for robot to rotate on it own axis and change directions.
        """
        all_directions = ['N', 'E', 'S', 'W']
        if each_move[0] == 'L':
            rotate_position =  (all_directions.index(self.direction)-int(each_move[1])) % 4
            self.direction = all_directions[rotate_position]
        else:
            rotate_position =  (all_directions.index(self.direction)+int(each_move[1])) % 4
            self.direction = all_directions[rotate_position]
        
    def forward_backward(self,each_move):
        """
        Logic for robot to move forward/backward 
        - This would change the position of robot resulting in calculation of number of steps.
        """
        if each_move[0] == 'F':
            if self.direction == 'N': # Default set to North
                self.position_b = self.position_b + int(each_move[1])
            elif self.direction == 'S':
                self.position_b = self.position_b - int(each_move[1])
            elif self.direction == 'E':
                self.position_a = self.position_a + int(each_move[1])
            elif self.direction == 'W':
                self.position_a = self.position_a - int(each_move[1])
        else:
            if self.direction == 'N':
                self.position_b = self.position_b - int(each_move[1])
            elif self.direction == 'S':
                self.position_b = self.position_b + int(each_move[1])
            elif self.direction == 'E':
                self.position_a = self.position_a - int(each_move[1])
            elif self.direction == 'W':
                self.position_a = self.position_a + int(each_move[1])
    def distance(self):
        return  abs(self.position_a) + abs(self.position_b)

"""
Here we use argparse to accept input from user from command line.
- After considering users input as a list of moves splitted into strings based on comma.
- We send the whole list to the class for intial assumptions.
- Based on movement params('F','B','L','R') - Decision is made if the 'robot' would move or rotate.
- Lastly for steps calculations - absolute distance robot has travelled on both x and y axis is added.

"""
parser = argparse.ArgumentParser(
            prog='moves.py',
            description='CLI Movement robot. Refer to README.md',
            formatter_class=argparse.RawDescriptionHelpFormatter,)
parser.add_argument('--input', '-i', type=str,
            required=True,
            help="a string of comma-separated commands eg: F1,R1,B2,L1,B3") 
nonparsed_input = parser.parse_args()
_input = nonparsed_input.input
input_list = _input.split(',')
get_movements = cli(input_list)
get_distance = get_movements.distance()
sys.stdout.write(str(get_distance)+" steps")
