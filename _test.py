import subprocess
import json

py_file = "moves.py"
def test_cli():
    command = ["python3", py_file, "-i"]
    test_count = 0
    with open("dummy_input.json") as user_inputs:
        test_cases = json.load(user_inputs)
        for test_case in test_cases['testCases']:
            cmd_with_input = command + [test_case['command']]
            result = subprocess.run(cmd_with_input,
                                    capture_output=True,
                                    text=True)
            out = result.stdout
            err = result.stderr
            exitcode = result.returncode
            assert exitcode == test_case['returnCode']
            assert out == test_case['output']
            assert err.__contains__(test_case['error'])